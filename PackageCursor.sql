CREATE OR REPLACE PACKAGE curs_pkg IS
    CURSOR emp_curs IS
    SELECT
        employee_id
    FROM
        employees
    ORDER BY
        employee_id;

    PROCEDURE open_curs;

    FUNCTION fetch_n_rows (
        n NUMBER := 1
    ) RETURN BOOLEAN;

    PROCEDURE close_curs;

END curs_pkg;