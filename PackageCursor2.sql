CREATE OR REPLACE PACKAGE BODY curs_pkg IS

    PROCEDURE open_curs IS
    BEGIN
        IF NOT emp_curs%isopen THEN
            OPEN emp_curs;
        END IF;
    END open_curs;

    FUNCTION fetch_n_rows (
        n NUMBER := 1
    ) RETURN BOOLEAN IS
        emp_id employees.employee_id%TYPE;
    BEGIN
        FOR count IN 1..n LOOP
            FETCH emp_curs INTO emp_id;
            EXIT WHEN emp_curs%notfound;
            dbms_output.put_line('Id: ' ||(emp_id));
        END LOOP;

        RETURN emp_curs%found;
    END fetch_n_rows;

    PROCEDURE close_curs IS
    BEGIN
        IF emp_curs%isopen THEN
            CLOSE emp_curs;
        END IF;
    END close_curs;

END curs_pkg;