    declare
    v_first_name varchar2(20);

v_last_name varchar2(20);

BEGIN
    BEGIN
        v_first_name := 'Carmen';
        v_last_name := 'Miranda';
        dbms_output.put_line(v_first_name
                             || ' '
                             || v_last_name);
    END;

    dbms_output.put_line(v_first_name
                         || ' '
                         || v_last_name);
END;