CREATE OR REPLACE PROCEDURE add_dept IS
    v_dept_id    departments.department_id%TYPE;
    v_dept_name  departments.department_name%TYPE;
BEGIN
    v_dept_id := 280;
    v_dept_name := 'ST-Curriculum';
    INSERT INTO departments (
        department_id,
        department_name
    ) VALUES (
        v_dept_id,
        v_dept_name
    );

    dbms_output.put_line('Inserted '
                         || SQL%rowcount
                         || ' row.');
END;

BEGIN
add_dept;
END;