DECLARE
    v_country_name countries.country_name%TYPE := 'Korea, South';
    v_reg_id countries.region_id%TYPE;
BEGIN
    SELECT region_id INTO v_reg_id
        FROM countries WHERE country_name = v_country_name;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN --Si el registro no tiene datos
        DBMS_OUTPUT.PUT_LINE ('Country name ' || v_country_name || ',
        cannot be found. Re-enter the country name using the correct
        spelling.');
END;