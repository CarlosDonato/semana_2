DECLARE
    v_more_rows_exist BOOLEAN := true;
BEGIN
    curs_pkg.open_curs;--1
        LOOP
        v_more_rows_exist := curs_pkg.fetch_n_rows(3); --2
                dbms_output.put_line('-------');
        EXIT WHEN NOT v_more_rows_exist;
    END LOOP;

    curs_pkg.close_curs; --3
END;