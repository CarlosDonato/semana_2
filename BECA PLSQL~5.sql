declare cursor cur_ loc IS SELECT * FROM locations;
CURSOR c
ur_dept (p_locid NUMBER) IS
SELECT * FROM departments WHERE location_id = p_locid;
BEGIN
FOR v_locrec IN c
ur_loc
LOOP
DBMS_OUTPUT.PUT_LINE(v_locrec.city);
FOR v_deptrec IN c ur_dept(v_locrec.location_id) LOOP
    dbms_output.put_line(v_deptrec.department_name);
END LOOP;
END LOOP;
END;