CREATE OR REPLACE PROCEDURE query_emp 
( p_id IN employees.employee_id%TYPE,
  p_name OUT employees.last_name%TYPE,
  p_salary OUT employees.salary%TYPE ) 
  IS
BEGIN
    SELECT
        last_name,
        salary
    INTO
        p_name,
        p_salary
    FROM
        employees
    WHERE
        employee_id = p_id;

END query_emp;

DECLARE
    a_emp_name employees.last_name%TYPE;
    a_emp_sal employees.salary%TYPE;
BEGIN
    query_emp(178, a_emp_name, a_emp_sal);
    dbms_output.put_line('Name: ' || a_emp_name);
    dbms_output.put_line('Salary: ' || a_emp_sal);
END;