DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name,
        location_id
    FROM
        departments;

    v_department_id     departments.department_id%TYPE;
    v_department_name   departments.department_name%TYPE;
    v_location_id       departments.location_id%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name,
            v_location_id;
        EXIT WHEN cur_depts%notfound;
 DBMS_OUTPUT.PUT_LINE(v_department_id||' '||v_department_name);
END LOOP;
CLOSE cur_depts;
END;