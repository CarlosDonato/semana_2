CREATE OR REPLACE PROCEDURE raise_salary (
    p_id       IN  employees.employee_id%TYPE,
    p_percent  IN  NUMBER
) IS
BEGIN
    UPDATE employees
    SET
        salary = salary * ( 1 + p_percent / 100 )
    WHERE
        employee_id = p_id;

END raise_salary;

CREATE OR REPLACE PROCEDURE process_employees IS
    CURSOR emp_cursor IS
    SELECT
        employee_id
    FROM
        employees;

BEGIN for
v_emp_rec IN emp_cursor LOOP
    raise_salary(v_emp_rec.employee_id, 10);
END LOOP;
END process_employees;

BEGIN
process_employees;
END;